from cStringIO import StringIO
import csv
import datetime

from django.contrib.admin import util as admin_util
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _


def write_row(row):
    """
    Writes a csv row to a temporary buffer and returns it as a string.
    """
    buffer = StringIO()
    csv.writer(buffer, dialect='excel').writerow([unicode(value).encode('utf8') for value in row])
    return buffer.getvalue()


class CSVExportAdminMixin(object):

    def get_csv_export_data(self, request, queryset):
        # exportable fields should be list of tuples containing (field name, column name)
        field_list, headers = zip(*self.export_fields)

        yield write_row(headers)

        for obj in queryset:
            line_values = []
            for field in field_list:
                field_obj, attr, value = admin_util.lookup_field(field, obj, self)
                line_values.append(value)
            yield write_row(line_values)

    def export_to_csv(self, request, queryset):
        csv_rows = self.get_csv_export_data(request, queryset)
        response = HttpResponse(csv_rows, mimetype='text/csv')
        response.streaming = True

        filename = '{model_name}-export-{date}.csv'.format(
            model_name=queryset.model.__name__.lower(),
            date=datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S'),
        )

        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response

    export_to_csv.short_description = _('Export to CSV')
