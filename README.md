CSV Export Admin
================


usage:

    # example/admin.py

    from csvexportadmin.admin import CSVExportAdminMixin


    class ExampleAdmin(CSVExportAdminMixin, admin.ModelAdmin):
        actions = ('export_to_csv', )
        
        # ... usual model admin stuffs ...

        # define export_fields with (field_name, column_name):
        export_fields = (
            ('name', 'Name')
            ('address1', 'Address 1'),
            ('address2', 'Address 2'),
            ('city', 'City'),
            ('province', 'Province'),
            ('postal_code', 'Postal Code'),
            ('phone', 'Phone'),
            ('calculated_field', 'See Below'),
        )

        def calculated_field(self, obj):
            return obj.something + 1


To export a csv, visit the models admin page and chose 'export csv' from admin
actions dropdown list.
